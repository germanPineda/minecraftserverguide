In order to get a new instance of ubuntu functioning properly first you need to update all packages

sudo apt-get update

once all the packges update we need three packages to get the minecraft server running

- Screen
- JDK
- JavaC

# Screen installation and configuration
[reference](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-screen-on-an-ubuntu-cloud-server)

- sudo apt-get install screen
- y

once done screen doesn't need any futher configuration and it's ready to use

# JavaC installation and configration
[reference](https://askubuntu.com/questions/117189/apt-get-install-openjdk-7-jdk-doesnt-install-javac-why)

sudo apt-get install openjdk-8-jdk

# JDK 17 over javac installation
[reference](https://petewan.com/blog-post/install-and-manage-multiple-java-jdk-and-jre-versions-on-ubuntu-20-04/)

apt install openjdk-17-jdk

## changing java sdk version

sudo update-alternatives --config javac 
number of choice

# Minecraft server installation and setup
[reference](https://www.spigotmc.org/wiki/buildtools/#1-18)

This step is made over a windows enviroment

- download BuildTools [here](https://hub.spigotmc.org/jenkins/job/BuildTools/)

Once downloaded create a .bat file and copy this text
Or you can use [this](install.bat)

    @echo off
    IF NOT EXIST BuildTools (
        mkdir BuildTools
    )
    cd BuildTools
    curl -z BuildTools.jar -o BuildTools.jar https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
    set /p Input=Enter the version: || set Input=latest
    java -jar BuildTools.jar --rev %Input%
    pause

You just have to select the desired version, in this case we used "latest" for minecraft 1.18

After everything's done you'll have something like this

![files](Files.PNG)

next step is to initialize the server file inside BuildTools folder

[reference](https://www.spigotmc.org/wiki/spigot-installation/)

- Paste the following text into a text document. Save it as start.bat in the same directory as spigot.jar:
  

        @echo off
        java -Xms#G -Xmx#G -XX:+UseG1GC -jar spigot.jar nogui
        pause
    (where # is your allocated server memory in GB)
- Double click the batch file

for practical puposes is recommended to have two files inside the creation folder

## [`eula.txt`](eula.txt)

    #By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula).
    #Sat Jun 12 02:42:57 UTC 2021
    eula=true

## [`start.sh`](start.sh)

    screen -S mserver java -Xmx7168M -Xms1768M -Djava.net.preferIPv4Stack=true -jar spigot-1.18.jar nogui

over firezilla just need to upload all the files

once all is uploaded just need two commands

    cd %folderName%
    sh start.sh

And... you'll have a full configurated minecraft server

maybe you'll want to edit some values over `server.properties` file like:
- Gamemode
- difficulty
- max-players
- online-mode
- view-distance
  
and so on

I recommend having one or more op players to help de server administration